import random
import tkinter as tk


def draw_point(canvas, x, y, color='red', r=5):
    canvas.create_oval(x - r, y - r, x + r, y + r, fill=color, outline='')

    draw_point(20, 60, r=10)
    draw_point(color='green', x=50, y=40)
    draw_point(r=7, color='blue', x=30, y=20)


class Program(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        self.hi_there = tk.Button(self)
        self.hi_there["text"] = "Hello World\n(click me)"
        self.hi_there["command"] = self.say_hi
        self.hi_there.pack(side="top")

        self.quit = tk.Button(self, text="QUIT", fg="red",
                              command=self.master.destroy)
        self.quit.pack(side="bottom")

    def say_hi(self):
        print("hi there, everyone!")

    def nn(self):
        """
        :return: Random number between 0 and 300
        """
        return random.randrange(0, 300)

    def nf(self):
        """
        Example: '#e791f3', '#f1f93d'
        :return: Random color in hexa
        """
        return f'#{random.randrange(256 ** 3) :06x}'


if __name__ == '__main__':
    root = tk.Tk()
    app = Program(master=root)
    app.nn()
    app.mainloop()
