import random as rand
import tkinter as tk


class Game:
    def __init__(self):
        # Initializing the widgets and the variables you want to access from multiple methods
        self.root = tk.Tk()
        self.root.geometry("450x250+500+300")
        self.frame = tk.Frame(self.root)
        self.frame.pack()
        self.correctAnswer = None
        self.root.title("Let's play Math!")
        self.root.welcomeLabel = tk.Label(text="LET'S PLAY MATH!").pack()
        self.startLabel = tk.Label(text='Select a math operation to start').pack()
        self.addBtn = tk.Button(text="Addition", command=lambda: self.calculate("Add"))
        self.addBtn.place(x=160, y=60)
        self.subBtn = tk.Button(text="Subtraction", command=lambda: self.calculate("Sub"))
        self.subBtn.place(x=235, y=60)
        self.correctAnswer = None
        self.answerEntry = tk.Entry()
        self.answerEntry.focus_set()
        self.checkAnswerLbl = tk.Label(text="Let's try again.")
        self.restartBtn = tk.Button(text="Play Again?", command=self.playAgain)
        self.isCorrectBtn = tk.Button(text="Check Answer", command=self.isCorrect)
        self.QuestionLbl = tk.Label()

    def genRandom(self):
        a = rand.randrange(1, 10, 1)
        b = rand.randrange(1, 10, 1)
        return a, b

    def calculate(self, method):
        self.playAgain()  # This is to clear the widgets if you press one of the buttons before you give a correct answer
        ranNumber = self.genRandom()
        if method == "Add":
            self.correctAnswer = int(ranNumber[0] + ranNumber[1])
            calcString = "+"
        elif method == "Sub":
            self.correctAnswer = int(ranNumber[0] - ranNumber[1])
            calcString = "-"
        self.QuestionLbl.config(text="What does {0} {1} {2} equal?".format(ranNumber[0], calcString, ranNumber[1]))
        self.QuestionLbl.place(x=0, y=125)
        self.answerEntry.place(x=300, y=125)
        self.isCorrectBtn.place(x=300, y=150)

    def isCorrect(self):
        try:
            answerEntered = int(self.answerEntry.get())
        except ValueError:
            # If it's not an integer, you get a ValueError, which we catch to prevent the program from producing an error.
            # You could make another widget that displays some kind of response here when they put in input that doesn't compute, but I'm just making it so it doesn't respond to it here.
            return
        if answerEntered != self.correctAnswer:
            self.checkAnswerLbl.place(x=150, y=125)
        elif answerEntered == self.correctAnswer:
            self.checkAnswerLbl.config(text="Hooray!")
            self.checkAnswerLbl.place(x=150, y=125)
        self.restartBtn.place(x=300, y=200)

    def playAgain(self):
        # We're hiding the appropriate widgets and clearing the text of the Entry widget.
        self.answerEntry.place_forget()
        self.answerEntry.delete(0, "end")
        self.restartBtn.place_forget()
        self.checkAnswerLbl.place_forget()
        self.isCorrectBtn.place_forget()
        self.QuestionLbl.place_forget()


if __name__ == '__main__':  # This means, only run the following code if we're running this file directly (rather than from another module)
    game = Game()  # We're creating an object called game that is an instance of the Game class.
    game.root.mainloop()
