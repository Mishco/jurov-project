import random
from tkinter import messagebox, RAISED, SUNKEN, Tk, Button, Label


class Game:
    """
    Object Game for the status of the current game
    """

    def __init__(self):
        self.clicked_on = None
        self.buttons = {}
        self.inputs = {}
        self.a_value = 0
        self.b_value = 0
        self.c_value = 0
        self.d_value = 0


def create_random_list():
    """
    Create random list of numbers without duplicate
    :return:
    """
    return random.sample(range(1, 100), 16)


def check_equation(game):
    """
    Check if equation is right or not
    :return: if right 'HOTOVO' else nothing
    """
    a = game.a_value
    b = game.b_value
    c = game.c_value
    d = game.d_value
    # check if all values are putted
    if a != 0 and b != 0 and c != 0 and d != 0:
        print('a: {0}'.format(a))
        print('b: {0}'.format(b))
        print('c: {0}'.format(c))
        print('d: {0}'.format(d))
        if a + b == c + d:
            print('HOTOVO')
            messagebox.showinfo("Find4Numbers", "HOTOVO")


def wait_for_click_on_input(alphabet, game):
    """
    Pick up some number and place on empty label
    :param alphabet: on which button you would like to place number
    :param game: actual state of game
    :return:
    """
    if game.clicked_on is not None:
        clicked_btn = game.buttons.get(game.clicked_on)

        # Check with add number is there is already some number
        value_in = game.inputs.get(alphabet)['text']
        if value_in != ' [ ] ':
            print("Here is already number {}".format(game.inputs.get(alphabet)['text']))
            return

        game.inputs.get(alphabet).config(text=clicked_btn['text'])
        game.clicked_on = None
        clicked_btn.place_forget()

        if alphabet is 'A':
            game.a_value = clicked_btn['text']
        elif alphabet is 'B':
            game.b_value = clicked_btn['text']
        elif alphabet is 'C':
            game.c_value = clicked_btn['text']
        elif alphabet is 'D':
            game.d_value = clicked_btn['text']

        check_equation(game)
    else:
        idx = game.inputs.get(alphabet)['text']
        num = get_key(idx, game.buttons)

        print('returning number: {} to place with index {}'.format(game.inputs.get(alphabet)['text'], num))

        returned_btn = game.buttons.get(num)
        returned_btn.config(relief=RAISED)
        returned_btn.config(bg='blue')
        game.inputs.get(alphabet).config(text=' [ ] ')


def get_key(val, my_dict):
    """
    Get key from dictionary if you know value
    :param val: value
    :param my_dict: from which dictionary
    :return: key
    """
    for key, value in my_dict.items():
        if val == my_dict.get(key)['text']:
            return key

    return "key doesn't exist"


def press(num, game):
    """
    Press on number button
    :param num: index of the button
    :param game: state of parameters
    :return: clicked button or nothing if button was already clicked
    """
    if game.clicked_on is None:
        game.clicked_on = num
        game.buttons.get(num).config(relief=SUNKEN)
        game.buttons.get(num).config(bg='red')
    else:
        print("Already clicked on: {} number".format(game.clicked_on))


def create_buttons(color_bg, color_fg, _width, game, rnd_numbers, gui):
    """
    Create list of buttons
    :param color_bg: background color
    :param color_fg: foreground color
    :param _width: width of button
    :param game: parameters
    :param rnd_numbers: series of random numbers without duplicate
    :param gui:
    :return:
    """

    game.buttons[1] = Button(gui, text=rnd_numbers[0], fg=color_fg, bg=color_bg,
                             command=lambda: press(1, game), height=1, width=_width)
    game.buttons[1].grid(row=0, column=0)

    game.buttons[2] = Button(gui, text=rnd_numbers[1], fg=color_fg, bg=color_bg,
                             command=lambda: press(2, game), height=1, width=_width)
    game.buttons[2].grid(row=0, column=1)

    game.buttons[3] = Button(gui, text=rnd_numbers[2], fg=color_fg, bg=color_bg,
                             command=lambda: press(3, game), height=1, width=_width)
    game.buttons[3].grid(row=1, column=0)
    game.buttons[3].visible = True

    game.buttons[4] = Button(gui, text=rnd_numbers[3], fg=color_fg, bg=color_bg,
                             command=lambda: press(4, game), height=1, width=_width)
    game.buttons[4].grid(row=1, column=1)

    game.buttons[5] = Button(gui, text=rnd_numbers[4], fg=color_fg, bg=color_bg,
                             command=lambda: press(5, game), height=1, width=_width)
    game.buttons[5].grid(row=2, column=0)

    game.buttons[6] = Button(gui, text=rnd_numbers[5], fg=color_fg, bg=color_bg,
                             command=lambda: press(6, game), height=1, width=_width)
    game.buttons[6].grid(row=2, column=1)

    game.buttons[7] = Button(gui, text=rnd_numbers[6], fg=color_fg, bg=color_bg,
                             command=lambda: press(7, game), height=1, width=_width)
    game.buttons[7].grid(row=3, column=0)

    game.buttons[8] = Button(gui, text=rnd_numbers[7], fg=color_fg, bg=color_bg,
                             command=lambda: press(8, game), height=1, width=_width)
    game.buttons[8].grid(row=3, column=1)

    game.buttons[9] = Button(gui, text=rnd_numbers[8], fg=color_fg, bg=color_bg,
                             command=lambda: press(9, game), height=1, width=_width)
    game.buttons[9].grid(row=4, column=0)

    game.buttons[10] = Button(gui, text=rnd_numbers[9], fg=color_fg, bg=color_bg,
                              command=lambda: press(10, game), height=1, width=_width)
    game.buttons[10].grid(row=4, column=1)

    game.buttons[11] = Button(gui, text=rnd_numbers[10], fg=color_fg, bg=color_bg,
                              command=lambda: press(11, game), height=1, width=_width)
    game.buttons[11].grid(row=5, column=0)

    game.buttons[12] = Button(gui, text=rnd_numbers[11], fg=color_fg, bg=color_bg,
                              command=lambda: press(12, game), height=1, width=_width)
    game.buttons[12].grid(row=5, column=1)

    game.buttons[13] = Button(gui, text=rnd_numbers[12], fg=color_fg, bg=color_bg,
                              command=lambda: press(13, game), height=1, width=_width)
    game.buttons[13].grid(row=6, column=0)

    game.buttons[14] = Button(gui, text=rnd_numbers[13], fg=color_fg, bg=color_bg,
                              command=lambda: press(14, game), height=1, width=_width)
    game.buttons[14].grid(row=6, column=1)

    game.buttons[15] = Button(gui, text=rnd_numbers[14], fg=color_fg, bg=color_bg,
                              command=lambda: press(15, game), height=1, width=_width)
    game.buttons[15].grid(row=7, column=0)

    game.buttons[16] = Button(gui, text=rnd_numbers[15], fg=color_fg, bg=color_bg,
                              command=lambda: press(16, game), height=1, width=_width)
    game.buttons[16].grid(row=7, column=1)


def create_inputs_labels(color_bg, color_fg, _width, game, rnd_numbers, gui):
    """
    Create inputs labels where are numbers are posted
    :param gui:
    :param game:
    :param _width:
    :param rnd_numbers:  series of random numbers without repeating
    :param color_bg: color of background
    :param color_fg: color of foreground
    :return:
    """
    plus = Label(gui, text=' + ', fg='black', bg=color_fg, height=1, width=4)
    plus.grid(row=4, column=4)
    minus = Label(gui, text=' + ', fg='black', bg=color_fg, height=1, width=4)
    minus.grid(row=4, column=8)
    game.inputs['A'] = Button(gui, text=' [ ] ', fg=color_fg, bg=color_bg,
                              command=lambda: wait_for_click_on_input("A", game), height=1, width=_width)
    game.inputs['A'].grid(row=4, column=3)

    game.inputs['B'] = Button(gui, text=' [ ] ', fg=color_fg, bg=color_bg,
                              command=lambda: wait_for_click_on_input("B", game), height=1, width=_width)
    game.inputs['B'].grid(row=4, column=5)

    game.inputs['C'] = Button(gui, text=' [ ] ', fg=color_fg, bg=color_bg,
                              command=lambda: wait_for_click_on_input("C", game), height=1, width=_width)
    game.inputs['C'].grid(row=4, column=7)

    game.inputs['D'] = Button(gui, text=' [ ] ', fg=color_fg, bg=color_bg,
                              command=lambda: wait_for_click_on_input("D", game), height=1, width=_width)
    game.inputs['D'].grid(row=4, column=9)

    equals = Label(gui, text=' = ', fg='black', bg=color_fg, height=1, width=_width)
    equals.grid(row=4, column=6)


def start():
    gui = Tk()
    gui.configure(background="white")
    gui.title("Find 4 Numbers")
    gui.geometry("400x240")

    game = Game()
    color_bg = 'blue'
    color_fg = 'white'
    _width = 5

    rnd_numbers = create_random_list()
    create_buttons(color_bg, color_fg, _width, game, rnd_numbers, gui)
    create_inputs_labels(color_bg, color_fg, _width, game, rnd_numbers, gui)

    gui.mainloop()


if __name__ == '__main__':
    start()
